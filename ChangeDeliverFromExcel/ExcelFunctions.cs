﻿using ChangeDeliverFromExcel.Models;
using ChangeDeliverFromExcel.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace ChangeDeliverFromExcel
{
    public static class ExcelFunctions
    {

        public static List<ExcelOrderRow> GetExcelRows(string path)
        {
            List<ExcelOrderRow> result = new List<ExcelOrderRow>();
            string outFileName = "";

            Excel.Application xlApp = null;
            Excel.Workbook xlWorkbook = null;
            Excel._Worksheet xlWorksheet = null;
            Excel.Range xlRange = null;

            try
            {
                if(Path.GetExtension(path) != ".csv")
                {
                    xlApp = new Excel.Application();
                    xlWorkbook = xlApp.Workbooks.Open(path);
                    xlWorksheet = xlWorkbook.Sheets[1];
                    xlRange = xlWorksheet.UsedRange;

                    outFileName = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                    xlWorkbook.SaveAs(outFileName, Excel.XlFileFormat.xlCSVWindows);

                    GC.Collect();
                    GC.WaitForPendingFinalizers();

                    //release com objects to fully kill excel process from running in the background
                    Marshal.ReleaseComObject(xlRange);
                    Marshal.ReleaseComObject(xlWorksheet);

                    //close and release
                    xlWorkbook.Close();
                    Marshal.ReleaseComObject(xlWorkbook);

                    //quit and release
                    xlApp.Quit();
                    Marshal.ReleaseComObject(xlApp);
                }
                else // If fila already is .csv we only copy it
                {
                    outFileName = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                    File.Copy(path, outFileName, true);
                }

                using (StreamReader sr = File.OpenText(outFileName))
                {
                    string s = String.Empty;
                    while ((s = sr.ReadLine()) != null)
                    {
                        string[] line = s.Split(',');

                        if(line.Length >= 3)
                        {
                            ExcelOrderRow eor = new ExcelOrderRow();

                            eor.OrderNo = line[0].Trim();
                            eor.ProductNo = line[1].Trim();
                            eor.DeliverDate = line[2].Trim();
                            
                            if(line.Length > 3)
                            {
                                eor.OriginalDeliverDate = line[3].Trim();
                            }

                            if (line.Length > 4)
                            {
                                eor.UpdatedOrderRow = line[4].Trim();
                            }

                            result.Add(eor);
                        }
                    }
                }

                File.Delete(outFileName);
            }
            catch (Exception e)
            {

            }
            finally
            {

            }

            return result;
        }
    }
}
