﻿using GIS_Dto;
using GISClientLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChangeDeliverFromExcel
{
    public class GarpFunctions
    {
        OrderModel mOM;
        GenericTableModel mGTM;

        public GarpFunctions()
        {
            try
            {
                mOM = new OrderModel("", true);
                mGTM = new GenericTableModel("", true);

            }
            catch (Exception e)
            {
            }
        }
            
        public string UpdateGarp(string order, string row, string date)
        {
            string originaldate = "";

            try
            {
                UpdateTableIndexObject uo = new UpdateTableIndexObject();

                List<FieldObject> idxFields = new List<FieldObject>();

                uo.TableName = "OGR";
                uo.Key = order.PadRight(6) + row.PadLeft(3);
                
                uo.Fields = new List<FieldObject>();
                uo.Fields.Add(new FieldObject { FieldName = "LDT", FieldValue = date });


                mGTM.UpdateTableByIndex(uo, "false");

                originaldate = "";
            }
            catch (Exception e)
            {

            }

            return originaldate;
        }

        public List<OrderRow> GetOrderRows(string orderno)
        {
            List<OrderRow> result = new List<OrderRow>();

            try
            {
                
                result = mOM.GetOrderRowList(1, orderno, "*", false, 0, false);

            }
            catch (Exception e)
            {

            }

            return result;
        }

        public OrderRow GetOrderRow(string orderno, string rowno)
        {
            OrderRow result = new OrderRow();

            try
            {

                result = mOM.GetOrderRow(orderno, rowno);

            }
            catch (Exception e)
            {

            }

            return result;
        }
    }
}
