﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChangeDeliverFromExcel.Models
{
    public class WorkerObject
    {
        public BindableCollection<ExcelOrderRow> ExcelRows { get; set; }
        public string ExcelPath { get; set; }
    }
}
