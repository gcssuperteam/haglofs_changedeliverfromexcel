﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChangeDeliverFromExcel.Models
{
    public class ExcelOrderRow
    {
        public string OrderNo { get; set; }
        public string DeliverDate { get; set; }
        public string OriginalDeliverDate { get; set; }
        public string ProductNo { get; set; }
        public string UpdatedOrderRow { get; set; }
        public string Message { get; set; }
    }
}
