﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChangeDeliverFromExcel.Models
{
    public class WorkerResult
    {
        public WorkerResult()
        {
            try
            {
                ExcelRows = new BindableCollection<ExcelOrderRow>();
            }
            catch (Exception e)
            {
            }

        }

        public BindableCollection<ExcelOrderRow> ExcelRows { get; set; }
        public string LogPath { get; set; }
    }
}
