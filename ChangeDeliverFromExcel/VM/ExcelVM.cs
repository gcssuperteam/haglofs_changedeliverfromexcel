﻿using Caliburn.Micro;
using ChangeDeliverFromExcel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChangeDeliverFromExcel.VM
{
    public  class ExcelVM : Screen
    {
        //*****************
        // **** SIZES *****
        //*****************
        private BindableCollection<ExcelOrderRow> _ExcelRows;
        public BindableCollection<ExcelOrderRow> ExcelRows
        {
            get
            {
                return _ExcelRows;
            }
            set
            {
                _ExcelRows = value;
                NotifyOfPropertyChange(() => ExcelRows);
            }
        }

        //*****************
        // **** LOGS *****
        //*****************
        private BindableCollection<ExcelOrderRow> _LogRows;
        public BindableCollection<ExcelOrderRow> LogRows
        {
            get
            {
                return _LogRows;
            }
            set
            {
                _LogRows = value;
                NotifyOfPropertyChange(() => LogRows);
            }
        }
    }
}
