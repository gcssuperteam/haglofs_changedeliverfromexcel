﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChangeDeliverFromExcel
{
    public class LogFunctions
    {
        private StreamWriter writer;

        public string LogfilePath { get; set; }

        public LogFunctions(string filename)
        {
            try
            {

                string directory = App.Configuration["LogPath"] ;
                string file = filename + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".csv";

                LogfilePath = Path.Combine(directory, file);

                if(!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
            }
            catch (Exception e)
            {
            }

            writer = new StreamWriter(LogfilePath);
        }

        public bool WriteToLogfile(string order, string product, string date, string originaldate, string rowno, string message)
        {
            bool result = false;

            try
            {
                writer.WriteLine(order.Trim() + "," + product.Trim() + "," + date.Trim() + "," + originaldate.Trim() + "," + rowno.Trim() + "," + message.Trim());
                result = true;
            }
            catch (Exception)
            {

            }

            return result;
        }

        public void CloseLogFile()
        {
            try
            {
                writer.Flush();
                writer.Close();
            }
            catch (Exception e)
            {

            }

        }

    }
}
