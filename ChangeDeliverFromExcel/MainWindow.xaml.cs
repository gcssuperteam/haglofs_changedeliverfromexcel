﻿using ChangeDeliverFromExcel.Models;
using ChangeDeliverFromExcel.VM;
using GIS_Dto;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using log4net;

namespace ChangeDeliverFromExcel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private GarpFunctions mGF = null;
        private delegate void UpdateProgressBarDelegate(System.Windows.DependencyProperty dp, Object value);

        public MainWindow()
        {
            InitializeComponent();
            log4net.Config.XmlConfigurator.Configure();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                mGF = new GarpFunctions();
                this.DataContext = new ExcelVM();
                log.Debug("Application loaded");
            }
            catch (Exception ex)
            {

            }
        }

        private void btnOpenExcel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string path = "";
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "Excel files (*.xl;*.xlsx;*.xls;*.csv)|*.xl;*.xlsx;*.xls;*.xlt;*.csv|All files (*.*)|*.*";

                if (dialog.ShowDialog() == true)
                    path = dialog.FileName;

                labelOpenExcelFile.Text = path;

                var dc = ((ExcelVM)this.DataContext);

                dc.LogRows?.Clear();
                dc.ExcelRows = new Caliburn.Micro.BindableCollection<Models.ExcelOrderRow>();

                foreach (var row in ExcelFunctions.GetExcelRows(path))
                {
                    dc.ExcelRows.Add(row);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void btnExecute_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (System.IO.File.Exists(labelOpenExcelFile.Text))
                {
                    btnExecute.IsEnabled = false;
                    btnOpenExcel.IsEnabled = false;


                    var dc = ((ExcelVM)this.DataContext);
                    pgbUpdate.Minimum = 0;
                    pgbUpdate.Maximum = dc.ExcelRows.Count;
                    pgbUpdate.Value = 0;

                    BackgroundWorker worker = new BackgroundWorker();
                    worker.WorkerReportsProgress = true;
                    if(System.IO.Path.GetExtension(labelOpenExcelFile.Text) == ".csv")
                    {
                        worker.DoWork += worker_DoWork_row;
                    }
                    else
                    {
                        worker.DoWork += worker_DoWork_product;
                    }

                    
                    worker.ProgressChanged += worker_ProgressChanged;
                    worker.RunWorkerCompleted += worker_Completed;

                    worker.RunWorkerAsync(new WorkerObject { ExcelRows = dc.ExcelRows, ExcelPath = labelOpenExcelFile.Text });
                }
                else
                {
                    MessageBox.Show("Cant find Excel file!");
                }
            }
            catch (Exception ex)
            {
                btnExecute.IsEnabled = true;
                btnOpenExcel.IsEnabled = true;
            }
        }

        void worker_DoWork_product(object sender, DoWorkEventArgs e)
        {
            LogFunctions log = null;

            WorkerObject workerObject = (WorkerObject)e.Argument;
            e.Result = new WorkerResult();

            try
            {
                int idx = 0;
                log = new LogFunctions(System.IO.Path.GetFileNameWithoutExtension(workerObject.ExcelPath));
                ((WorkerResult)e.Result).LogPath = log.LogfilePath;

                foreach (var row in workerObject.ExcelRows)
                {
                    try
                    {
                        // Check if deliverdate is present, if not we skip thos row
                        if (!string.IsNullOrEmpty(row.DeliverDate))
                        {
                            // Get all the rows on order with requested productno (sku)
                            var rows = mGF.GetOrderRows(row.OrderNo);

                            if(rows.Count > 0)
                            {
                                rows = rows.Where(r=>r.ProductNo == row.ProductNo).ToList();

                                if(rows.Count > 0)
                                {
                                    // Loop each rows that as found and set the new deliverdate, and also log changes tahat was made
                                    foreach (OrderRow or in rows)
                                    {
                                        try
                                        {
                                            mGF.UpdateGarp(or.OrderNo, or.RowNo.ToString(), row.DeliverDate);

                                            row.OriginalDeliverDate = or.PreferedDeliverDate;
                                            log.WriteToLogfile(row.OrderNo, row.ProductNo, row.DeliverDate, row.OriginalDeliverDate, or.RowNo.ToString(), "");
                                        }
                                        catch (Exception ex)
                                        {

                                        }

                                        ((WorkerResult)e.Result).ExcelRows.Add(row);
                                    }
                                }
                                else
                                {
                                    row.Message = "PRODUCT_MISSING";
                                    log.WriteToLogfile(row.OrderNo, row.ProductNo, row.DeliverDate, "", "", "PRODUCT_MISSING");
                                    ((WorkerResult)e.Result).ExcelRows.Add(row);
                                }
                            }
                            else // Order not found
                            {
                                row.Message = "ORDER_MISSING";
                                log.WriteToLogfile(row.OrderNo, row.ProductNo, row.DeliverDate, "", "", "ORDER_MISSING");
                                ((WorkerResult)e.Result).ExcelRows.Add(row);
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                    idx++;

                    (sender as BackgroundWorker).ReportProgress(idx);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (log != null)
                {
                    log.CloseLogFile();
                }
            }
        }

        /// <summary>
        /// Reading of logfiles. This is used to reverse updates that are done
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void worker_DoWork_row(object sender, DoWorkEventArgs e)
        {
            LogFunctions log = null;

            WorkerObject workerObject = (WorkerObject)e.Argument;
            e.Result = new WorkerResult();

            try
            {
                int idx = 0;
                log = new LogFunctions(System.IO.Path.GetFileNameWithoutExtension(workerObject.ExcelPath));

                foreach (var row in workerObject.ExcelRows)
                {
                    try
                    {
                        // Check if deliverdate is present, if not we skip thos row
                        if (!string.IsNullOrEmpty(row.OriginalDeliverDate))
                        {
                            // Get all the rows on order with requested productno (sku)
                            var garpRow = mGF.GetOrderRow(row.OrderNo, row.UpdatedOrderRow.ToString());

                            // Loop each rows that as found and set the new deliverdate, and also log changes tahat was made
                            if(garpRow != null)
                            {
                                try
                                {
                                    //row.OriginalDeliverDate = garpRow.PreferedDeliverDate;
                                    mGF.UpdateGarp(garpRow.OrderNo, garpRow.RowNo.ToString(), row.OriginalDeliverDate);
                                    
                                    //log.WriteToLogfile(row.OrderNo, row.ProductNo, row.DeliverDate, row.OriginalDeliverDate, garpRow.RowNo.ToString());
                                }
                                catch (Exception ex)
                                {

                                }
                                
                                ((WorkerResult)e.Result).ExcelRows.Add(row);
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                    idx++;

                    (sender as BackgroundWorker).ReportProgress(idx);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (log != null)
                {
                    log.CloseLogFile();
                }
            }
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pgbUpdate.Value = e.ProgressPercentage;
        }

        void worker_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                btnExecute.IsEnabled = true;
                btnOpenExcel.IsEnabled = true;

                var dc = ((ExcelVM)this.DataContext);

                dc.LogRows = ((WorkerResult)e.Result).ExcelRows;
                labelLogFile.Text = ((WorkerResult)e.Result).LogPath;

                pgbUpdate.Value = 0;

                MessageBox.Show("Update is done!");
            }
            catch (Exception ex)
            {
            }
        }

    }
}
